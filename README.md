This project uses the three.js lib to add 3D elements to an HTML page. More infos on this lib can be found at https://threejs.org/.

So far, the project create a planet with a block on its top that you can rotate using the arrow keys. There is a character represented by a block and a sphere 
on top of it next to the planet. These Two main elements are separeted in different Groups so that they can be manipulated separetely.
You can also use the mouse to turn the entiere world, even though this feature isn't supposed to be in the final release.

To use this project, just clone it and load the index.html page on your browser.
Languages used : html, css, js.